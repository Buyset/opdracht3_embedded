#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <fcntl.h>
#include <sys/sendfile.h>

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    ssize_t len;
    int fd;
    int sent_bytes = 0;
    char file_size[256];
    struct stat file_stat;
    int offset;
    int remain_data;
    char buffer[256];
    char FILE_TO_SEND[256];

    /* Look if the commandline has all parameters */
    if (argc < 4) {
       fprintf(stderr,"usage %s hostname port filename\n", argv[0]);
       exit(0);
    }

    /* Making connection with the server */
    portno = (int) strtol(argv[2], (char **)NULL, 10);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0){
        error("ERROR opening socket");
    }         
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0){
        error("ERROR connecting");
    } 

    /* Search if the file exist */  
    bzero(FILE_TO_SEND,256);
    strcpy(FILE_TO_SEND, argv[3]);
    printf("\r\nFile name: %s\r\n", FILE_TO_SEND);

    fd = open(FILE_TO_SEND, O_RDONLY);
    if (fd == -1)
    {
            fprintf(stderr, "Error opening file\r\n");

            exit(EXIT_FAILURE);
    }

    /* send the name of the file */
    n = write(sockfd,FILE_TO_SEND,18);

    /* wait for a signal to start sending the file */
    bzero(buffer,256);
    n = read(sockfd,buffer,255);    

    /* Get file stats */
    if (fstat(fd, &file_stat) < 0)
    {
            fprintf(stderr, "Error fstat\r\n");

            exit(EXIT_FAILURE);
    }

    fprintf(stdout, "File Size: \n%d bytes\n", file_stat.st_size);

    sprintf(file_size, "%d", file_stat.st_size);

    /* Sending file size */
    len = send(sockfd, file_size, sizeof(file_size), 0);
    if (len < 0)
    {
            fprintf(stderr, "Error on sending greetings\r\n");

            exit(EXIT_FAILURE);
    }

    fprintf(stdout, "Server sent %d bytes for the size\n", len);

    offset = 0;
    remain_data = file_stat.st_size;

    /* Sending file data */
    while (((sent_bytes = sendfile(sockfd, fd, &offset, BUFSIZ)) > 0) && (remain_data > 0))
    {
            fprintf(stdout, "1. Client sent %d bytes from file's data, offset is now : %d and remaining data = %d\n", sent_bytes, offset, remain_data);
            remain_data -= sent_bytes;
            fprintf(stdout, "2. Client sent %d bytes from file's data, offset is now : %d and remaining data = %d\n", sent_bytes, offset, remain_data);
    }

    /* Reading the message from the server */
    bzero(buffer,256);
    n = read(sockfd,buffer,255);
    if (n < 0){
        error("ERROR reading from socket");
    }         
    printf("%s\n",buffer);

    close(sockfd);
    return 0;
}